Vue.component('ui-select', {
    props: [
        'variant',
        'variantArr'
    ],
    
    methods: {
        onChangeItem($event) {
            this.$emit('ui-select-change', $event.target.value)
        }
    },

    template: `
        <select 
            class="custom-select" :value="variant"
            @change="onChangeItem($event)">
            <option 
                v-for="variantItem in variantArr" :value="variantItem"
                >{{ variantItem }}
            </option>
        </select>
    `
})


new Vue({
    el: '#app',
    data: {
        variant: 'primary',
        variantArr: [
            'primary',
            'secondary',
            'success',
            'danger',
            'warning',
            'info',
            'light',
            'dark'
        ]
    },
    methods: {
        onChange: function(variant) {
            this.variant = variant
        }
    },
    computed: {
        alertVariant () {
            return 'alert-' + this.variant
        }
    }
})